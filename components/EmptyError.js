import React from 'react';
import { Message } from 'semantic-ui-react';

const EmptyMessage = (props) => (
  <Message attached negative>
    <Message.Header>{props.error_message}</Message.Header>
  </Message>
)

export default EmptyMessage;