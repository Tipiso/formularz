import React from 'react';

const LeftDoorWidth = (props) => {
    const {handleChange, offHover, onHover, onBlur, onFocus, values} = props;
    const errorMessage = 'Input cannot be empty, or longer than 3 digits';

    return ( 
    <>   
    <input
        name={'mlWidth'}
        placeholder={values.format === 1 ? "Door's width..." : "Left door..."}
        id={values.errors.mlWidth ? 'error-message' : null}
        onChange={handleChange('mlWidth')}
        onMouseOver={() => onHover('mlWidthHover')}
        onFocus={() => onFocus('mlWidthFocused')}
        onBlur={onBlur}
        onMouseLeave={offHover}
        type="number"
        defaultValue={values.mlWidth}
    />
    {values.errors.mlWidth && <span className='error'>{errorMessage}</span>}  
    </>
    );
}

export default LeftDoorWidth;