import React, { PureComponent } from 'react';
import { Form, Button } from 'semantic-ui-react';
import '../styles/Format.css';
import Formats from '../assets/formats.json';
import EmptyError from './EmptyError.js';

class Format extends PureComponent {
    state = {
        error: false,
    }

    saveAndContinue = (e) => {
        e.preventDefault()
        if (this.props.values.format)
            this.props.nextStep()
        else if (!this.props.values.format) {
            this.setState({
                empty: true,
            })
        }
    }

    componentDidUpdate() {
      if (this.props.values.format && this.state.error) {
            this.setState({
                empty: false,
            })
        }
    }

    render() {
       const list = Formats.formats.map(format => (
            <button 
                key={format.id}
                id={format.id}
                className={'ui button doors'}
                onClick={this.props.handleClick('format', format.id)}
                style={{border: `${this.props.checkSelection(format.id)}`}}
                defaultValue={this.props.values.format}             
                required>
            </button>
            
        ))
        return (
        <div>
            <Form >
                <h1 className="ui centered">Choose doors format:</h1>
                <Form.Field>
                {list}
                </Form.Field>
                <div className="submit">
                {<Button onClick={this.saveAndContinue}>Save And Continue</Button>}   
                </div>
                {this.state.empty && <EmptyError error_message="You have to choose a door format."/>}  
            </Form>
        </div>
        );
    }
}

export default Format;