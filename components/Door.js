import React from 'react';

const Door = (props) => {
    const {hoverState, focusedState, format} = props.values;
    let door;

    if(format === '1'){
    door = <div style={{backgroundImage: `url(${require('../images/sizes/singleSizes.PNG')})`}} className={'editable'}>
                <div className={(hoverState || focusedState)}></div>
           </div>
    }else if(format === '2'){
    door = <div style={{backgroundImage: `url(${require('../images/sizes/doubleSizes.PNG')})`}} className={'editable double'}>
           <div id={hoverState || focusedState}></div>
           </div>
    }else if(format === '3'){
    door = <div style={{backgroundImage: `url(${require('../images/sizes/double+topSizes.PNG')})`}} className={'editable double top'}>
           <div id={hoverState || focusedState}></div>
           </div>
    }else if(format === '4'){
    door = <div style={{backgroundImage: `url(${require('../images/sizes/double+doublewindowSizes.PNG')})`}} className={'editable double doubleWindows'}>
           <div id={hoverState || focusedState}></div>
           </div>     
    }else if(format === '5'){
    door = <div style={{backgroundImage: `url(${require('../images/sizes/doubleLeft+topSize.PNG')})`}} className={'editable double top'}>
              <div id={hoverState || focusedState}></div>
           </div> 
    }else if(format === '6'){
       door = <div style={{backgroundImage: `url(${require('../images/sizes/double+doubleWindow+topSize.PNG')})`}} className={'editable double doubleWindows top'}>
              <div id={hoverState || focusedState}></div>
       </div>   
    }else if(format === '7'){
       door = <div style={{backgroundImage: `url(${require('../images/sizes/doubleLeftSize.PNG')})`}} className={'editable double'}>
              <div id={hoverState || focusedState}></div>
       </div> 
    }else if(format === '8'){
       door = <div style={{backgroundImage: `url(${require('../images/sizes/singleLeftArrowSizes.PNG')})`}} className={'editable'}>
              <div className={hoverState || focusedState}></div>
       </div>    
    }else if(format === '9'){
       door = <div style={{backgroundImage: `url(${require('../images/sizes/singleSizes.PNG')})`}} className={'editable'}>
              <div className={hoverState || focusedState}></div>
           </div>
    }else if(format === '10'){
       door = <div style={{backgroundImage: `url(${require('../images/sizes/singleLeft+doubleWindowSizes.PNG')})`}} className={'editable doubleWindows single'}>
              <div id={hoverState || focusedState}></div>
           </div>   
    }else if(format === '11'){
       door = <div style={{backgroundImage: `url(${require('../images/sizes/singleRight+doubleWindows+topSizes.PNG')})`}} className={'editable doubleWindows single top'}>
              <div id={hoverState || focusedState}></div>
       </div>   
    }else if(format === '12'){
       door = <div style={{backgroundImage: `url(${require('../images/sizes/singleSizes.PNG')})`}} className={'editable'}>
              <div className={hoverState || focusedState}></div>
       </div>   
    }else if(format === '13'){
       door = <div style={{backgroundImage: `url(${require('../images/sizes/singleRight+doubleWindows.PNG')})`}} className={'editable doubleWindows single'}>
              <div id={hoverState || focusedState}></div>
       </div>        
    }else if(format === '14'){
       door = <div style={{backgroundImage: `url(${require('../images/sizes/singleLeft+doubleWindow+topSizes.PNG')})`}} className={'editable doubleWindows single top'}>
              <div id={hoverState || focusedState}></div>
       </div>   
    }else if(format === '15'){
       door = <div style={{backgroundImage: `url(${require('../images/sizes/doubleRightSizes.PNG')})`}} className={'editable double'}>
              <div id={hoverState || focusedState}></div>
       </div>     
    }

    return <>
            {door}
           </>;
}

export default Door;