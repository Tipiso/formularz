import React from 'react';
import { List, Message } from 'semantic-ui-react';
import '../styles/Summary.css';
import Colors from '../assets/colors.json';

const Summary = (props) => {
    const {format, height, lWidth, mlWidth, rWidth, mrWidth, addHeight, lock, color, glass, totalHeight, totalWidth, price} = props.values;
    let sumHeight = props.sumHeight;
    let sumWidth = props.sumWidth;
    let lockModel, glassModel, formatImage, glassImage, lockImage, colorImage;
    
    colorImage = Colors.colors.filter(checkColor => parseInt(color) === checkColor.id);

    if(lock === '1'){ lockModel = 'Profil zimny eko 50 mm Fine'; lockImage = `${require('../images/cold50mmFine.jpg')}`}
    if(lock === '2'){ lockModel = 'Profil zimny eko 40 mm Fine'; lockImage = `${require('../images/coldEko40mmFine.jpg')}` }
    if(lock === '3'){ lockModel = 'Profil ciepły 52 mm Fine'; lockImage = `${require('../images/warm52mmFine.jpg')}`}
    
    if(glass === '1'){ glassModel = 'Zespolone obustronne bezpieczne'; glassImage = `${require('../images/complexBothSafe.jpg')}` }
    if(glass === '2'){ glassModel = 'Zespolone pojedyczne bezpieczne'; glassImage = `${require('../images/complexSingleSafe.jpg')}`}
    if(glass === '3'){ glassModel = 'Pojedyncze bezpieczne'; glassImage = `${require('../images/singleSafe.jpg')}`}

    if(format === '1') formatImage = `${require('../images/single.jpg')}`;
    if(format === '2') formatImage = `${require('../images/double.PNG')}`;
    if(format === '3') formatImage = `${require('../images/double+top.PNG')}`;


    return ( <Message className='summary' attached='top'>
        <Message.Header>Summary</Message.Header>
            <div className="bar">
            {format && <div className='container'>
                            <div className='format display' style={{backgroundImage: `url(${formatImage})`}}></div>
                       </div>}
            {lock &&<>
                        <div className="container">
                            <div className='lock display' style={{backgroundImage: `url(${lockImage})`}}></div>
                            <span>{lockModel}</span>
                        </div>
                    </>}
            {color && <>
                        <div className="container">
                            <div className='color display' style={{backgroundColor: colorImage[0].color}}></div>
                            <span>RAL {colorImage[0].ral}</span>
                        </div>
                        </>}
            {glass &&<> 
                        <div className="container">
                            <div className='glass display' style={{backgroundImage: `url(${glassImage})`}}></div>
                            <span>{glassModel}</span>
                        </div>
                    </>}
                <div className="text">
                    {height && <List.Item>Wysokość drzwi: {height}cm</List.Item>}
                    {lWidth &&<List.Item>Szerokość lewego okna: {lWidth}cm</List.Item>}
                    {mlWidth && <List.Item>Szerekość lewych drzwi: {mlWidth}cm</List.Item>}
                    {rWidth && <List.Item>Szerokość prawego okna: {rWidth}cm</List.Item>}
                    {mrWidth && <List.Item>Szerokość prawych drzwi: {mrWidth}cm</List.Item>}
                    {addHeight && <List.Item>Wysokość nadbudowanego okna: {addHeight}cm</List.Item>}
                    {price && <List.Item>Cena: {price.toFixed(2)}PLN</List.Item>}
                    {(sumHeight ? <List.Item>Suma wysokości: {parseInt(sumHeight)}cm</List.Item> : <List.Item>Suma wysokości:{isNaN(parseInt(totalHeight)) ? 0 : parseInt(totalHeight)}cm</List.Item>)}
                    {(sumWidth ? <List.Item>Suma szerokości: {parseInt(sumWidth)}cm</List.Item> : <List.Item>Suma wysokości:{isNaN(parseInt(totalWidth)) ? 0 : parseInt(totalWidth)}cm</List.Item> )}          
                </div>
            </div>
    </Message>
    );
}

export default Summary;