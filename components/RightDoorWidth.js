import React from 'react';

const RightDoorWidth = (props) => {
    const {handleChange, offHover, onHover, onBlur, onFocus, values} = props;
    const errorMessage = 'Input cannot be empty, or longer than 3 digits';
    const rightInput = {right: '-100%'}
 
 
    return (
    <>
        <input
            name={'mrWidth'}
            placeholder="Right door..."
            id={values.errors.mrWidth ? 'error-message' : null}
            onChange={handleChange('mrWidth')}
            onMouseOver={() => onHover('mrWidthHover')}
            onMouseLeave={offHover}
            onFocus={() => onFocus('mrWidthFocused')}
            onBlur={onBlur}
            type="number"
            maxLength="3"
            defaultValue={values.mrWidth}
        />
        {values.errors.mrWidth && <span style={rightInput} className='error'>{errorMessage}</span>} 
    </>
    );
}

export default RightDoorWidth;