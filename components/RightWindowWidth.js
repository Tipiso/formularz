import React from 'react';

const RightWindowWidth = (props) => {
    const {handleChange, offHover, onHover, onBlur, onFocus, values} = props;
    const errorMessage = 'Input cannot be empty, or longer than 3 digits';
    const rightInput = {right: '-100%'}

    return (
    <>
        <input
            name={'rWidth'}
            placeholder="Right window..."
            id={values.errors.rWidth ? 'error-message' : null}
            onChange={handleChange('rWidth')}
            onMouseOver={() => onHover('rWidthHover')}
            onMouseLeave={offHover}
            onFocus={() => onFocus('rWidthFocused')}
            onBlur={onBlur}
            type="number"
            maxLength="3"
            defaultValue={values.rWidth}
        />
        {values.errors.rWidth && <span style={rightInput} className='error'>{errorMessage}</span>} 
    </>
    );
}
export default RightWindowWidth;