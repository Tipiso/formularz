import React, { PureComponent } from 'react';
import { Form, Button, TextArea } from 'semantic-ui-react';
import '../styles/Finalization.css';
import EmptyError from './EmptyError.js';

class Finalization extends PureComponent {
    state = {
        name: '',
        surname: '',
        companyName: '',
        streetPlace: '',
        local: null,
        postNumber: null,
        post: '',
        province: '',
        phone: null,
        email: '',
        companyTransport: '',
        selfPickup: '',
        montageInstallation: false,
        service: false,
        info: '',
        empty: false,
    }

    error_messages = {
        name: 'Name cannot be empty, or contain numbers',
        surname: 'Surname cannot be empty, or contain numbers',
        phone: 'Phone number cannot be empty',
        email: 'Email has to be in proper format: example@domain.com',
    }

    saveAndContinue = (e) => {
        const {name, surname, companyName, streetPlace, local, postNumber, post, province, phone, email} = this.state;

        if(!name || !surname || !companyName || !streetPlace || !local || !postNumber || !post || !province || !phone || !email){
            this.setState({
                empty: true,
            })
        }else{
            e.preventDefault();
            this.props.nextStep();
        }
    }

    back = (e) => {
        e.preventDefault();
        this.props.prevStep();
    }

    generateForm = (props) =>{
    const {errors} = props.values;

    const submitElements = <>
        <Button onClick={this.back}>Back</Button>
        <Button onClick={this.saveAndContinue}>Save And Continue </Button>
    </>

    const name = <Form.Field>
        <input
            placeholder='Name...'
            style={errors.name ? {border:'1px red solid'}: null}
            onChange={(props.handleChange('name'))}
            defaultValue={props.values.clientInfo.name}
            type="text"
            max="12"
        />
        {errors.name && <span style={{color: 'red'}}>{this.error_messages.name}</span>}
    </Form.Field>

    const surname = <Form.Field>
        <input
            placeholder='Surname...'
            style={errors.surname ? {border:'1px red solid'}: null}
            onChange={props.handleChange('surname')}
            type="text"
            max="30"
            defaultValue={props.values.clientInfo.surname}
        />
        {errors.surname && <span style={{color: 'red'}}>{this.error_messages.surname}</span>}
    </Form.Field>

    const companyName = <Form.Field>
        <input
            placeholder='Company...'
            onChange={props.handleChange('companyName')}
            type="text"
            max="30"
            defaultValue={props.values.clientInfo.companyName}
        />
    </Form.Field>

    const streetPlace = <Form.Field>
        <input
            placeholder='Street...'
            onChange={props.handleChange('streetPlace')}
            type="text"
            maxLength="40"
            defaultValue={props.values.clientInfo.streetPlace}
        />
    </Form.Field>

    const local = <Form.Field>
        <input
            placeholder='Local...'
            onChange={props.handleChange('local')}
            type="number"
            maxLength="3"
            defaultValue={props.values.clientInfo.local}
        />
    </Form.Field>

    const postNumber = <Form.Field>
        <input
            placeholder='Zip code...'
            onChange={props.handleChange('postNumber')}
            type="number"
            maxLength="10"
            defaultValue={props.values.clientInfo.postNumber}
        />
    </Form.Field>

    const post = <Form.Field>
        <input
            placeholder='Post...'
            onChange={props.handleChange('post')}
            type="text"
            maxLength="30"
            defaultValue={props.values.clientInfo.post}
        />
    </Form.Field>

    const province = <Form.Field>
        <input
            placeholder="Province..."
            onChange={props.handleChange('province')}
            type="text"
            maxLength="30"
            defaultValue={props.values.clientInfo.province}
        />
    </Form.Field>

    const phone = <Form.Field>
        <input  
            placeholder='Phone'
            style={errors.phone ? {border:'1px red solid'}: null}
            onChange={props.handleChange('phone')}
            type="number"
            maxLength="12"
            defaultValue={props.values.clientInfo.phone}
        />
        {errors.phone && <span style={{color: 'red'}}>{this.error_messages.phone}</span>}
    </Form.Field>

    const email = <Form.Field>
        <input  
            placeholder='Email...'
            style={errors.email ? {border:'1px red solid'}: null}
            onChange={props.handleChange('email')}
            type="email"
            defaultValue={props.values.clientInfo.email}
        />
        {errors.email && <span style={{color: 'red'}}>{this.error_messages.email}</span>}
    </Form.Field>

    const transport =
    <>
        <Form.Field>
            <label htmlFor='1'>Company Transport</label>
            <input
                type='radio'
                id='1'
                name='transport'
                value="companyTransport"
                // checked={this.state.companyTransport === true}
                onChange={props.handleChange('transport')}
                defaultChecked={props.values.clientInfo.companyTransport}  
            />
        </Form.Field>
        <Form.Field>
            <label htmlFor="2">Self Pickup</label>
            <input
                type='radio'
                id='2'
                name='transport'
                value="selfPickup"
                onChange={props.handleChange('transport')}
                defaultChecked={props.values.clientInfo.selfPickup}  
            />
    </Form.Field>
    </>

    const montageInstallation = <Form.Field>
        <label>Montage and installation</label>
        <input
            type='checkbox'
            name="montage"
            defaultChecked={props.values.clientInfo.montageInstallation}
            onChange={props.handleChange('montageInstallation')}
        />
    </Form.Field>

    const service = <Form.Field>
        <label>Service</label>
        <input
            type='checkbox'
            name='service'
            defaultChecked={props.values.clientInfo.service}
            onChange={props.handleChange('service')}
        />
    </Form.Field>

    const info = <Form.Field>
        <label>Comments</label>
        <TextArea
            onChange={props.handleChange('info')}
            defaultValue={props.values.clientInfo.info}
        />
    </Form.Field>

    return(
        <>
    <h2 className='ui header'>Orderer data</h2>
    <div className='client'>
        {name}
        {surname}
        {companyName}
        {streetPlace}
        {local}
        {postNumber}
        {post}
        {province}
    </div>
    <h2>Contact info</h2>
    <div className='contact'>
        {phone}
        {email}
    </div>
    <h2>Additional options</h2>
    <div className='options'>
        <div className="transport">
            {transport}
        </div>
        <div className="services">    
            {service}
            {montageInstallation}
        </div>
        <div className="comments">
            {info}
        </div>
    </div>
    <div className="submit">
        {submitElements}
    </div>
        </>
    )
    }

    render() {
        

        return (
        <div>
            <Form onKeyPress={() => this.setState({empty: false,})}>
            {this.generateForm(this.props)}
            </Form>
            {this.state.empty && <EmptyError error_message="You have to fill all needed inputs."/>}  
        </div>);
    }
}
export default Finalization;









