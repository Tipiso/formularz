import React, { PureComponent } from 'react';
import { Button, Form } from 'semantic-ui-react';
import Colors from '../assets/colors.json';
import '../styles/Color.css';
import EmptyError from './EmptyError.js';

class Color extends PureComponent {
    state = {
        empty: false,
    }
    price;

    saveAndContinue = (e) => {
        e.preventDefault()
        if (this.props.values.color){
            this.props.nextStep();
            this.props.updatePrice(this.price);
        }
        else if (!this.props.values.color) {
            this.setState({
                empty: true,
            })
        }
    }

    back = (e) => {
        e.preventDefault();
        this.props.prevStep();
    }

    render() {
        const { values, handleClick } = this.props;
        const colorPrice = 25;
        this.price = parseInt(values.doorSize) * colorPrice;
     
        let list = Colors.colors.map(color => (       
            <div key={color.id} className='color container'>
            <button
                className='color'
                key={color.color}
                name="color"
                alt="kolor"
                defaultValue={values.color}
                id={color.id}
                type="button"
                onClick={handleClick('color', color.id)}
                style={{ backgroundColor: color.color, border: `${this.props.checkSelection(color.id)}` }}>
            </button>
            <span>RAL{color.ral}</span>  
            </div>  
            ))

        return (
        <div>
            <Form>
                <h1>Choose a color for your doors:</h1>
                <Form.Field style={{display:'flex', flexDirection:'row', flexWrap:'wrap'}}>
                    {list}
                </Form.Field>
                <div className="submit">
                    <Button onClick={this.back}>Back</Button>
                    <Button onClick={this.saveAndContinue}>Save And Continue </Button>
                </div>
            </Form>
            {this.state.empty && <EmptyError error_message="You have to choose a color for your door." />}
        </div>
        );
    }
}

export default Color;