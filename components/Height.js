import React from 'react';

const Height = (props) => {
    const {handleChange, offHover, onHover, onBlur, onFocus, values} = props;
    const errorMessage = 'Input cannot be empty, or longer than 3 digits';

    return (       
    <>
    {values.errors.height && <span className='error'>{errorMessage}</span>}
        <input
            name={'height'}
            id={values.errors.height ? 'error-message' : null}
            placeholder="Height..."
            onChange={(handleChange('height'))}
            onMouseOver={() => onHover('heightHover')}
            onMouseLeave={offHover}
            onFocus={() => onFocus('heightFocused')}
            onBlur={onBlur}
            defaultValue={values.height}
            type="number"
            autoComplete='off'
        />
    </>         
    );
}

export default Height;