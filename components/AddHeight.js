import React from 'react';

const AddHeight = (props) => {
    const {handleChange, offHover, onHover, onBlur, onFocus, values} = props;
    const errorMessage = 'Input cannot be empty, or longer than 3 digits';
    const rightInput= {right: '-100%'};

    return (<>
            <input
                name={'addHeight'}
                placeholder="Transom..."
                id={values.errors.addHeight ? 'error-message' : null}
                onChange={handleChange('addHeight')}
                onMouseOver={() => onHover('addHeightHover')}
                onMouseLeave={offHover}
                onFocus={() => onFocus('addHeightFocused')}
                onBlur={onBlur}
                type="number"
                maxLength="3"
                defaultValue={values.addHeight}
            />
            {values.errors.addHeight && <span style={rightInput} className='error'>{errorMessage}</span>}
            </>);
}

export default AddHeight;