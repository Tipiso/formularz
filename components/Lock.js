import React, { PureComponent } from 'react';
import { Button, Form } from 'semantic-ui-react';
import '../styles/Lock.css';
import EmptyError from './EmptyError.js';

const locks = [{
    id: 1,
    image: 'image1',
    price: 123,

},
{
    id: 2,
    image: 'image2',
    price: 234,

},
{
    id: 3,
    image: 'image3',
    price:256,

}];

class Lock extends PureComponent {
    state = {
        empty: false,
    }

    price;

    saveAndContinue = (e) => {
        e.preventDefault();
        if (this.props.values.lock){
            this.props.nextStep()
            this.props.updatePrice(this.price);
        }
        else if (!this.props.values.lock) {
            this.setState({
                empty: true,
            })
        }
    }

    back = (e) => {
        e.preventDefault();
        this.props.prevStep();
    }


    render() {
        const { values, handleClick } = this.props;

        let lock = locks.filter(checkLock => parseInt(values.lock) === checkLock.id);

        if(lock.length !== 0){
            this.price = values.doorSize * (lock[0].price);
            };

        const list = locks.map(lock => (
            <button
                key={lock.id}
                id={lock.id}
                className='ui button locks'
                onClick={handleClick('lock', lock.id)}
                style={{border: `${this.props.checkSelection(lock.id)}`}}
                alt="zamek"
                defaultValue={values.lock}
                required>
            </button>
        
        ))

        return (
        <div>
            <Form>
                    <h1 className="ui centered">Choose a lock for your doors:</h1>
                    <Form.Field className='lock'>
                        {list}
                    </Form.Field>
                <div className='submit'>
                    <Button onClick={this.back}>Back</Button>
                    <Button onClick={this.saveAndContinue}>Save And Continue </Button>    
                </div>       
            </Form>
            {this.state.empty && <EmptyError error_message="You have to choose a door profile."/>}  
        </div>
        )
    }
}

export default Lock;