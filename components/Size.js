import React, { PureComponent } from 'react';
import { Form, Button } from 'semantic-ui-react';
import '../styles/Size.css';
import Door from './Door.js';
import EmptyError from './EmptyError.js';
import Formats from '../assets/formats.json';
import Height from './Height';
import LeftDoorWidth from './LeftDoorWidth';
import RightDoorWidth from './RightDoorWidth';
import AddHeight from './AddHeight';
import LeftWindowWidth from './LeftWindowWidth';
import RightWindowWidth from './RightWindowWidth';

class Size extends PureComponent {

    state = {
        height: null,
        lWidth: null,
        rWidth: null,
        mlWidth: null,
        mrWidth: null,
        addHeight: null,
        empty: false,
    }

    error_messages = {
        rWidth: 'Input cannot be empty, or longer than 3 digits',
        mlWidth: 'Input cannot be empty, or longer than 3 digits',
        mrWidth: 'Input cannot be empty, or longer than 3 digits',
        addHeight: 'Input cannot be empty, or longer than 3 digits',
    }

    sumWidth;
    sumHeight;
    price; 
    doorSize;

    saveAndContinue = (e) => {
        e.preventDefault()
        const { values } = this.props;
        this.calculatePrice();

        if (parseInt(values.format) === 1 || parseInt(values.format) === 8 || parseInt(values.format) === 9 || parseInt(values.format) === 12) {
            if (!values.height || !values.mlWidth) {
                this.setState({empty: true})
            } else if (!values.errors.height && !values.errors.mlWidth) {
                this.props.nextStep()
                this.props.handleUpdate(this.sumWidth, this.sumHeight, this.doorSize);
                this.props.updatePrice(this.price);
            }
        } else if (parseInt(values.format) === 2 || parseInt(values.format) === 7 || parseInt(values.format) === 15) {
            if (!values.height || !values.mlWidth || !values.mrWidth) {
                this.setState({empty: true,})
            } else if (!values.errors.height && !values.errors.mlWidth && !values.errors.mrWidth) {
                this.props.nextStep()
                this.props.handleUpdate(this.sumWidth, this.sumHeight, this.doorSize);
                this.props.updatePrice(this.price);
            }
        } else if (parseInt(values.format) === 3 || parseInt(values.format) === 5) {
            if (!values.height || !values.mlWidth || !values.mrWidth || !values.addHeight) {
                this.setState({empty: true,})
            } else if (!values.errors.height && !values.errors.mlWidth && !values.errors.mrWidth && !values.errors.addHeight) {
                this.props.nextStep()
                this.props.handleUpdate(this.sumWidth, this.sumHeight, this.doorSize);
                this.props.updatePrice(this.price);
            }
        }else if(parseInt(values.format) === 4){
            if (!values.height || !values.mlWidth || !values.mrWidth || !values.rWidth || !values.lWidth) {
                this.setState({empty: true,})
            } else if (!values.errors.height && !values.errors.mlWidth && !values.errors.mrWidth && !values.errors.rWidth && !values.errors.lWidth) {
                this.props.nextStep()
                this.props.handleUpdate(this.sumWidth, this.sumHeight, this.doorSize);
                this.props.updatePrice(this.price);
            }
        }else if(parseInt(values.format) === 6){
            if(!values.height || !values.mlWidth || !values.mrWidth || !values.rWidth || !values.lWidth || !values.addHeight){
                this.setState({empty:true})
            }else if(!values.errors.height && !values.errors.mlWidth && !values.errors.mrWidth && !values.errors.rWidth && !values.errors.lWidth && !values.errors.addHeight){
                this.props.nextStep()
                this.props.handleUpdate(this.sumWidth, this.sumHeight, this.doorSize);
                this.props.updatePrice(this.price);
            }
        }else if(parseInt(values.format) === 10 || parseInt(values.format) === 13 ){
            if(!values.height || !values.mlWidth || !values.rWidth || !values.lWidth){
                this.setState({empty:true})
            }else if(!values.errors.height && !values.errors.mlWidth && !values.errors.rWidth && !values.errors.lWidth){
                this.props.nextStep()
                this.props.handleUpdate(this.sumWidth, this.sumHeight, this.doorSize);
                this.props.updatePrice(this.price);
            }
        }else if(parseInt(values.format) === 11 || parseInt(values.format) === 14){
            if(!values.height || !values.mlWidth || !values.rWidth || !values.lWidth || !values.addHeight){
                this.setState({empty:true})
            }else if(!values.errors.height && !values.errors.mlWidth && !values.errors.rWidth && !values.errors.lWidth && !values.errors.addHeight){
                this.props.nextStep()
                this.props.handleUpdate(this.sumWidth, this.sumHeight, this.doorSize);
                this.props.updatePrice(this.price);
            }
        }
    }

    back = (e) => {
        e.preventDefault();
        this.props.prevStep();
    }

    generateHeight = () => (
        <Height
            values={this.props.values}
            handleChange={this.props.handleChange}
            onHover={this.props.onHover}
            offHover={this.props.offHover}
            onBlur={this.props.onBlur}
            onFocus={this.props.onFocus}/>
        )
    

    generateLeftDoorWidth = () =>(
        <LeftDoorWidth 
            values={this.props.values}
            handleChange={this.props.handleChange}
            onHover={this.props.onHover}
            offHover={this.props.offHover}
            onBlur={this.props.onBlur}
            onFocus={this.props.onFocus}/>
    )

    generateRightDoorWidth = () =>(
        <RightDoorWidth
            values={this.props.values}
            handleChange={this.props.handleChange}
            onHover={this.props.onHover}
            offHover={this.props.offHover}
            onBlur={this.props.onBlur}
            onFocus={this.props.onFocus}/>
    )

    generateRightWindowWidth = () =>(
        <RightWindowWidth
            values={this.props.values}
            handleChange={this.props.handleChange}
            onHover={this.props.onHover}
            offHover={this.props.offHover}
            onBlur={this.props.onBlur}
            onFocus={this.props.onFocus}/>
    )

    generateLefttWindowWidth = () =>(
        <LeftWindowWidth
            values={this.props.values}
            handleChange={this.props.handleChange}
            onHover={this.props.onHover}
            offHover={this.props.offHover}
            onBlur={this.props.onBlur}
            onFocus={this.props.onFocus}/>
    )

    generateAddHeight = () => (
        <AddHeight 
            values={this.props.values}
            handleChange={this.props.handleChange}
            onHover={this.props.onHover}
            offHover={this.props.offHover}
            onBlur={this.props.onBlur}
            onFocus={this.props.onFocus}/>
    )

    generateForm = (format) => {
    let form;
    const leftInput = {alignSelf:'flex-end'};
    const rightInput = {alignSelf:'flex-start'};

    if(parseInt(format) === 1 || parseInt(format) === 8 || parseInt(format) === 9 || parseInt(format) === 12){
        form = 
    <> 
     <div className="left">
        <Form.Field style={leftInput} className={'size'}>
            {this.generateHeight()}
        </Form.Field>
      </div> 
        <div className="center">
            <Door values={this.props.values}/>
            <Form.Field className={'size'}>
                {this.generateLeftDoorWidth()}
            </Form.Field>
             <div className="submit">
                <Button onClick={this.back}>Back</Button>
                <Button onClick={this.saveAndContinue}>Save And Continue </Button>
             </div>
        </div>
        <div className="right">
        </div>
    </>
    }

    if(parseInt(format) === 2 || parseInt(format) === 7 || parseInt(format) === 15){
        form =
        <>   
            <div className='left'>
            </div>
            <div className="center">
            <Form.Field className={'size'}>
                {this.generateHeight()} 
            </Form.Field>   
            <Door values={this.props.values}/>     
            <div className="input">
            <Form.Field className={'size'}>
                {this.generateLeftDoorWidth()}
            </Form.Field>
            <Form.Field className={'size'}>
                {this.generateRightDoorWidth()}         
            </Form.Field>    
                </div>
            <div className="submit">
                <Button className={''} onClick={this.back}>Back</Button>
                <Button className={''} onClick={this.saveAndContinue}>Save And Continue </Button>
            </div>
            </div>
            <div className="right">   
            </div>
    </>    
    }

    if(parseInt(format) === 3 || parseInt(format) === 5){
        form = 
        <>
            <div className="left">
                <Form.Field style={leftInput} className={'size'}>
                    {this.generateHeight()}   
                </Form.Field>       
            </div>
            <div className="center">
                <Form.Field className={'size'}>
                    {this.generateAddHeight()}
                </Form.Field>
            <Door values={this.props.values}/>
            <div className="input">
            <Form.Field className={'size'}>
                {this.generateLeftDoorWidth()}
            </Form.Field>     
            <Form.Field className={'size'}>
                {this.generateRightDoorWidth()}
            </Form.Field>    
                </div>      
                <div className="submit"> 
                    <Button className={''} onClick={this.back}>Back</Button>
                    <Button className={''} onClick={this.saveAndContinue}>Save And Continue </Button>
                </div>
            </div>
            <div className="right">
            </div>
        </>
    }

    if(parseInt(format) === 4){
       form = 
            <>
                <div className="left">
                    <Form.Field style={leftInput} className={'size'}>
                        {this.generateLefttWindowWidth()}
                    </Form.Field>       
                </div>
                <div style={{flexGrow:1}}className="center">
                    <Form.Field className={'size'}>
                        {this.generateHeight()}   
                    </Form.Field>
                    <Door values={this.props.values}/>
                <div className="input">
                    <Form.Field className={'size'}>
                        {this.generateLeftDoorWidth()}
                    </Form.Field>     
                    <Form.Field className={'size'}>
                        {this.generateRightDoorWidth()}
                    </Form.Field>    
                    </div>      
                    <div className="submit"> 
                        <Button className={''} onClick={this.back}>Back</Button>
                        <Button className={''} onClick={this.saveAndContinue}>Save And Continue </Button>
                    </div>
                </div>
                <div className="right">
                    <Form.Field style={rightInput} className={'size'}>
                        {this.generateRightWindowWidth()}
                    </Form.Field>
                </div>
            </>
    }
    if(parseInt(format) === 6){
        form =  <>
                <div className="left">
                    <Form.Field style={leftInput} className={'size'}>
                        {this.generateLefttWindowWidth()}
                    </Form.Field>       
                </div>
                <div className="center">
                    <div className="input">
                    <Form.Field className={'size'}>
                        {this.generateHeight()}   
                    </Form.Field>
                    <Form.Field className={'size'}>
                        {this.generateAddHeight()}  
                    </Form.Field>
                    </div>
                    <Door values={this.props.values}/>
                <div className="input">
                    <Form.Field className={'size'}>
                        {this.generateLeftDoorWidth()}
                    </Form.Field>     
                    <Form.Field className={'size'}>
                        {this.generateRightDoorWidth()}
                    </Form.Field>    
                    </div>      
                    <div className="submit"> 
                        <Button className={''} onClick={this.back}>Back</Button>
                        <Button className={''} onClick={this.saveAndContinue}>Save And Continue </Button>
                    </div>
                </div>
                <div className="right">
                    <Form.Field style={rightInput} className={'size'}>
                        {this.generateRightWindowWidth()}
                    </Form.Field>
                </div>
            </>
    }
    if(parseInt(format) === 10 || parseInt(format) === 13){
        form =
            <>
                <div className="left">
                    <Form.Field style={leftInput} className={'size'}>
                        {this.generateLefttWindowWidth()}
                    </Form.Field>       
                </div>
                <div className="center">
                    <Form.Field className={'size'}>
                        {this.generateHeight()}   
                    </Form.Field>
                    <Door values={this.props.values}/>
                <div className="input">
                    <Form.Field className={'size'}>
                        {this.generateLeftDoorWidth()}
                    </Form.Field>     
                    </div>      
                    <div className="submit"> 
                        <Button className={''} onClick={this.back}>Back</Button>
                        <Button className={''} onClick={this.saveAndContinue}>Save And Continue </Button>
                    </div>
                </div>
                <div className="right">
                    <Form.Field style={rightInput} className={'size'}>
                        {this.generateRightWindowWidth()}
                    </Form.Field>
                </div>
            </>  
    }
    if(parseInt(format) === 11 || parseInt(format) === 14){
        form =
            <>
                <div className="left">
                    <Form.Field style={leftInput} className={'size'}>
                        {this.generateLefttWindowWidth()}
                    </Form.Field>       
                </div>
                <div className="center">
                    <div className="input">
                        <Form.Field className={'size'}>
                            {this.generateHeight()}   
                        </Form.Field>
                        <Form.Field className={'size'}>
                            {this.generateAddHeight()}   
                        </Form.Field>
                    </div>
                    <Door values={this.props.values}/>
                <div className="input">
                    <Form.Field className={'size'}>
                        {this.generateLeftDoorWidth()}
                    </Form.Field>     
                    </div>      
                    <div className="submit"> 
                        <Button className={''} onClick={this.back}>Back</Button>
                        <Button className={''} onClick={this.saveAndContinue}>Save And Continue </Button>
                    </div>
                </div>
                <div className="right">
                    <Form.Field style={rightInput} className={'size'}>
                        {this.generateRightWindowWidth()}
                    </Form.Field>
                </div>
            </>
    }
    return form;
}
    
    calculatePrice = () =>{
    const { values } = this.props;
    this.sumHeight = (isNaN(parseInt(values.height)) ? 0 : (parseInt(values.height))) +
        (isNaN(parseInt(values.addHeight)) ? 0 : (parseInt(values.addHeight)));

    this.sumWidth = (isNaN(parseInt(values.mlWidth)) ? 0 : (parseInt(values.mlWidth))) +
        (isNaN(parseInt(values.mrWidth)) ? 0 : (parseInt(values.mrWidth))) +
        (isNaN(parseInt(values.lWidth)) ? 0 : (parseInt(values.lWidth))) +
        (isNaN(parseInt(values.rWidth)) ? 0 : (parseInt(values.rWidth)));

    this.doorSize = (parseFloat((this.sumHeight)/100) * parseFloat((this.sumWidth)/100));
    let pricePerMeter = Formats.formats.filter(checkFormat => parseInt(values.format) === checkFormat.id);
    this.price = parseFloat(((pricePerMeter[0].price * this.doorSize)).toFixed(2));
    }

    render() {  
        const form = this.generateForm(this.props.values.format);

            return (
                <div>
                <Form onKeyPress={() => this.setState({empty: false,})} className='size container'>
                {form}
                </Form>
                {this.state.empty && <EmptyError error_message={"You can't leave empty inputs"}/>}
                </div>                        
            )
    }
}
export default Size;



