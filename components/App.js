import React, { Component } from 'react';
import '../styles/App.css';
import Mainform from './Mainform';
import { Container } from 'semantic-ui-react';

class App extends Component {
  render() {
    return (
      <Container textAlign='center'>
        <Mainform />
      </Container>
    );
  }
}

export default App;
