import React, { PureComponent } from 'react';
import Format from './Format';
import Lock from './Lock';
import Color from './Color';
import Size from './Size';
import Summary from './Summary';
import Glass from './Glass';
import Finalization from './Finalization';
import Steps from './Steps.js';

class Mainform extends PureComponent {
    state = {
        step: 1,
        format:null,
        selectedItem: null,
        hoverState: '',
        focusedState: '',
        color: null,
        lock: null,
        glass: null,
        height: null,
        lWidth: null,
        rWidth: null,
        mlWidth: null,
        mrWidth: null,
        addHeight: null,
        totalHeight: null,
        totalWidth: null,
        price: null,
        doorSize: null,

        clientInfo:{
            name: '',
            surname: '',
            companyName: '',
            streetPlace: '',
            local: null,
            postNumber: null,
            post: '',
            province: '',
            phone: null,
            email: '',
            transport: '',
            montageInstallation: false,
            service: false,
            info: '',
        },
      
        errors: {
            format: false,
            lock: false,
            color: false,
            glass: false,
            height: false,
            lWidth: false,
            rWidth: false,
            mlWidth: false,
            mrWidth: false,
            addHeight: false,
            name: false,
            surname: false,
            companyName: false,
            streetPlace: false,
            local: false,
            postNumber: false,
            post: false,
            province: false,
            phone: false,
            email: false,
            info: false,
        }

    }

    validate = () => {
        let valid = true;

        if (this.state.step === 1) {
            if (this.state.errors.format === true) {
                valid = false
                return valid;
            }
            else if (this.state.errors.format === false)
                return valid;
        }

        if (this.state.step === 2) {
            if (this.state.errors.height === true || this.state.errors.mlWidth === true || this.state.errors.lWidth === true || this.state.errors.mrWidth === true || this.state.errors.rWidth === true || this.state.errors.addHeight === true) {
                valid = false
                return valid;
            } else if (this.state.errors.height === false && this.state.errors.mlWidth === false && this.state.errors.lWidth === false && this.state.errors.mrWidth === false && this.state.errors.rWidth === false && this.state.errors.addHeight === false)
                return valid;
        }

        if (this.state.step === 3) {
            if (this.state.errors.lock === true) {
                valid = false
                return valid;
            }
            else if (this.state.errors.lock === false)
                return valid;
        }

        if (this.state.step === 4) {
            if (this.state.errors.color === true) {
                valid = false
                return valid;
            }
            else if (this.state.errors.color === false)
                return valid;
        }

        if (this.state.step === 5) {
            if (this.state.errors.glass === true) {
                valid = false
                return valid;
            }
            else if (this.state.errors.glass === false)
                return valid;
        }
    }


    nextStep = () => {
        const { step } = this.state;
        if (this.validate(this.state.errors)) {
            this.setState({
                step: step + 1,
                selectedItem: null,
            })
        } else {
            return;
        };

    }

    //trzeba bedzie potestowac czy zerowac bledy
    prevStep = () => {
        const { step } = this.state
        this.setState((prevState) => ({
            step: step - 1,
            price: prevState.price,     
        }))
    }

    determineIfSelected = id =>{
    const isItemSelected = this.state.selectedItem === id;
    return isItemSelected ? "black 1px solid" : '';
    }

    handleChange = input =>  event => {
        const { format, lock, color, glass, height, mlWidth, lWidth, mrWidth, rWidth, addHeight, name, surname, companyName, streetPlace, local, postNumber, post, province, phone, email, info } = this.state.errors;
        let errors = { format, lock, color, glass, height, mlWidth, lWidth, mrWidth, rWidth, addHeight, name, surname, companyName, streetPlace, local, postNumber, post, province, phone, email, info };
        let value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

        const nameRegEx = /^[a-z '-]+$/i;
        const emailRegEx = /^\S+@\S+\.\S+$/;
        const phoneRegEx = /^[0-9]*$/;

        switch (input) {
            case 'height':
                if (value <= 0 || value.length > 3)
                    errors.height = true;
                else if (value > 0 && value.length <= 3)
                    errors.height = false;
                break;
            case 'mlWidth':
                if (value <= 0 || value.length > 3)
                    errors.mlWidth = true;
                else if (value > 0 && value.length <= 3)
                    errors.mlWidth = false;
                break;
            case 'lWidth':
                if (value <= 0 || value.length > 3)
                    errors.lWidth = true;
                else if (value > 0 && value.length <= 3)
                    errors.lWidth = false;
                break;
            case 'mrWidth':
                if (value <= 0 || value.length > 3)
                    errors.mrWidth = true;
                else if (value > 0 && value.length <= 3)
                    errors.mrWidth = false;
                break;
            case 'rWidth':
                if (value <= 0 || value.length > 3)
                    errors.rWidth = true;
                else if (value > 0 && value.length <= 3)
                    errors.rWidth = false;
                break;
            case 'addHeight':
                if (value <= 0 || value.length > 3)
                    errors.addHeight = true;
                else if (value > 0 && value.length <= 3)
                    errors.addHeight = false;
                break;
            case 'name':
                if(value.length === 0 || !nameRegEx.test(value)){
                    errors.name = true;
                }
                else if (value.length > 0 && nameRegEx.test(value))
                    errors.name = false;
                break;
            case 'surname':
                if(value.length === 0 || !nameRegEx.test(value)){
                    errors.surname = true;
                }
                else if (value.length > 0 && nameRegEx.test(value))
                    errors.surname = false;
                break;
            case 'email':
                if(value.length === 0 || !emailRegEx.test(value)){
                    errors.email = true;
                }
                else if (value.length > 1 && emailRegEx.test(value))
                    errors.email = false;
                break;
            case 'phone':
                if(value.length === 0 || !phoneRegEx.test(value)){
                    errors.phone = true;
                }
                else if (value.length > 1 && phoneRegEx.test(value))
                    errors.phone = false;
                break;
            default: break;
        }

        if(this.state.step < 6){
            this.setState({
                errors,
                [input]: value,
            })
        }

        if(this.state.step === 6){
             this.setState({
                errors,
               clientInfo:{
                ...this.state.clientInfo,
                [input]: value,
                }
            });
        }
    }

    onHover = (name) =>{  
    if(!this.state.hoverState){
    this.setState({
        ...this.state,
        hoverState: [name],
    })
    }
    }

    offHover = () =>{
    if(this.state.hoverState){
    this.setState({
        ...this.state,
        hoverState: '',
    })
    }
    }

    handleClick = (input, id) => event => {
        let value = event.currentTarget.id;
            this.setState({
                [input] : value,
                selectedItem: id,
                }
            )    
    }

    onFocus = (name) =>{
    if(!this.state.focusedState){
        this.setState({
            ...this.state,
            focusedState: name,
        })
    }
    }

    onBlur = () =>{
        this.setState({
            ...this.state,
            focusedState: ''
        })
    }

    handleUpdate = (width, height, doorSize) => {
        this.setState({
            totalHeight: width,
            totalWidth: height,
            doorSize: doorSize,
        })
    }

    updatePrice = price =>{
        this.setState({
            price: this.state.price + price,
        }, () => console.log(this.state.price))
    }

    render() {
        const { step, format, color, lock, glass, hoverState, focusedState, height, lWidth, price,
             rWidth, mlWidth, mrWidth, addHeight, progress, totalWidth, totalHeight, clientInfo, selectedItem, doorSize, errors } = this.state;

        const values = { step, format, color, lock, glass, hoverState, focusedState, height, lWidth, price,
             rWidth, mlWidth, mrWidth, addHeight, progress, totalHeight, totalWidth, selectedItem, doorSize, clientInfo, errors };

        switch (step) {
            case 1:
                return <>
                    <div className="ui centered"><Steps step={values.step}/></div>
                    <Format
                        nextStep={this.nextStep}
                        handleClick={this.handleClick}
                        checkSelection={this.determineIfSelected}
                        values={values} />
                </>
            case 2:
                return <>
                    <div><Steps step={values.step}/></div>
                    <Size
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange={this.handleChange}
                        values={values}
                        handleUpdate={this.handleUpdate}
                        onHover={this.onHover}
                        offHover={this.offHover}
                        onFocus={this.onFocus}
                        onBlur={this.onBlur}
                        updatePrice={this.updatePrice} />
                </>
            case 3:
                return <>
                    <div><Steps step={values.step}/></div>
                    <Lock
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleClick={this.handleClick}
                        checkSelection={this.determineIfSelected}
                        values={values}
                        handleUpdate={this.handleUpdate}
                        updatePrice={this.updatePrice} />
                </>
            case 4:
                return <>
                    <div><Steps step={values.step}/></div>
                    <Color
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleClick={this.handleClick}
                        checkSelection={this.determineIfSelected}
                        values={values}
                        updatePrice={this.updatePrice} />
                </>
            case 5:
                return <>
                    <div><Steps step={values.step}/></div>
                    <Glass
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleClick={this.handleClick}
                        values={values}
                        checkSelection={this.determineIfSelected} 
                        updatePrice={this.updatePrice}/>
                </>
            case 6:
                return<>
                    <div><Steps step={values.step}/></div> 
                    <Summary values={values} />
                        <Finalization
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange={this.handleChange}
                        values={values} />
                    </>
            default: return "ojej";
        }
    }
}

export default Mainform;