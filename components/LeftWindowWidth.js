import React from 'react';

const LeftWindowWidth = (props) =>{
const {handleChange, offHover, onHover, onBlur, onFocus, values} = props;
    const errorMessage = 'Input cannot be empty, or longer than 3 digits';

    return (
    <>
        <input
            name={'lWidth'}
            placeholder="Left window..."
            id={values.errors.lWidth ? 'error-message': null}
            onChange={handleChange('lWidth')}
            onMouseOver={() => onHover('lWidthHover')}
            onMouseLeave={offHover}
            onFocus={() => onFocus('lWidthFocused')}
            onBlur={onBlur}
            type="number"
            maxLength="3"
            defaultValue={values.lWidth}
        />
        {values.errors.lWidth && <span className='error'>{errorMessage}</span>} 
    </>
    );
    }
export default LeftWindowWidth;