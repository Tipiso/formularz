import React, { PureComponent } from 'react';
import { Button, Form } from 'semantic-ui-react';
import '../styles/Glass.css';
import EmptyError from './EmptyError.js';

const glass = [{
    id: 1,
    image: 'image1',
    price: 123
},
{
    id: 2,
    image: 'image2',
    price: 234
},
{
    id: 3,
    image: 'image3',
    price: 321
}];

class Glass extends PureComponent {
    state = {
        empty: false,
    }

    price;

    saveAndContinue = (e) => {
        e.preventDefault();
        if (this.props.values.glass){
            this.props.nextStep();
            this.props.updatePrice(this.price);
        }
        else if (!this.props.values.glass) {
            this.setState({
                empty: true,
            })
        }
    }

    back = (e) => {
        e.preventDefault();
        this.props.prevStep();
    }


    render() {
        const { values, handleClick } = this.props;

        let glassType = glass.filter(checkGlass => parseInt(values.lock) === checkGlass.id);

        if(glassType.length !== 0){
            this.price = values.doorSize * (glassType[0].price);
            };

        const list = glass.map(glass => (
            <button className='ui button glass'
                key={glass.id}
                id={glass.id}
                name="glass"
                onClick={handleClick('glass', glass.id)}
                style={{border: `${this.props.checkSelection(glass.id)}`}}
                alt="glass"
                type="image"
                defaultValue={values.glass}
                required></button>
        ))

        return (
        <div>
            <Form>
                    <h1 className="ui centered">Choose glass for your doors:</h1>
                    <Form.Field className='glass'>
                        {list}
                    </Form.Field>
                {this.state.error && <span style={{ color: 'red' }}>{this.messages.glass_incorrect}</span>}
                <div className="submit">
                    <Button onClick={this.back}>Back</Button>
                    <Button onClick={this.saveAndContinue}>Save And Continue </Button>
                </div>
            </Form>
            {this.state.empty && <EmptyError error_message="You have to choose glass for your door."/>}
        </div>
        )
    }
}

export default Glass;