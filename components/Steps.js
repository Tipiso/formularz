import React from 'react';
import { Icon, Step } from 'semantic-ui-react';
import '../styles/Steps.css';

const Steps = (props) => {
return(
    
  <Step.Group size='mini'>
    <Step active={props.step === 1} disabled={props.step !== 1}>
      <Icon name='columns' />
      <Step.Content>
        <Step.Title>Doors</Step.Title>
        <Step.Description>Choose your door format</Step.Description>
      </Step.Content>
    </Step>

    <Step active={props.step === 2} disabled={props.step !== 2}>
      <Icon name='expand arrows alternate' />
      <Step.Content>
        <Step.Title>Size</Step.Title>
        <Step.Description>Choose sizes of your door</Step.Description>
      </Step.Content>
    </Step>

    <Step active={props.step === 3} disabled={props.step !== 3}>
      <Icon name='lock' />
      <Step.Content>
        <Step.Title>Lock</Step.Title>
        <Step.Description>Choose your lock</Step.Description>
      </Step.Content>
    </Step>

    <Step active={props.step === 4} disabled={props.step !== 4}>
    <Icon name='paint brush' />
    <Step.Content>
    <Step.Title>Color</Step.Title>
    <Step.Description>Choose your color</Step.Description>
    </Step.Content>
    </Step>

    <Step active={props.step === 5} disabled={props.step !== 5}>
    <Icon name='clone outlines' />
    <Step.Content>
    <Step.Title>Glass</Step.Title>
    <Step.Description>Choose your glass type</Step.Description>
    </Step.Content>
    </Step>

    <Step active={props.step === 6} disabled={props.step !== 6}>
    <Icon name='info' />
    <Step.Content>
    <Step.Title>Confirm Order</Step.Title>
    <Step.Description>Choose your shipment info</Step.Description>
    </Step.Content>
    </Step>
    </Step.Group> 
)
}
export default Steps